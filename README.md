# EGMP Offtake Assessment for the Svalbard Population of Pink-footed Goose

## Description
Data and model files for the 2023 EGMP offtake assessment of the Svalbard Population of Pink-footed Goose.

## Year
2023

## Assessor
Fred Johnson, EGMP Data Centre

## How to cite this material
https://doi.org/10.5281/zenodo.8112774  

## Data
Data used in this assessment is available from the EGMP database (https://calm-dune-07f6d4603.azurestaticapps.net/ ), and can be used under the conditions stated in the EGMP Database Policy. Descriptions of the raw datasets are likewise available from the website.

